﻿# HostMonitorWeb
[![build status](https://git.peterkeuter.nl/peter/hostmonitorweb/badges/master/pipeline.svg)](https://git.peterkeuter.nl/peter/hostmonitorweb/commits/master)

This application will read the status from HostMonitor and show it in a dashboard-like way. It works by getting the status-updates from HostMonitor.

There is also a heartbeat endpoint, which if enabled (see __Configuration__ below), will allow HostMonitor to send a heartbeat event, to automatically check the HostMonitor connection. 
This endpoint is available on `<endpoint>/api/hostmonitor/heartbeat`. It is a simple `POST` without any content.

## Docker

This application is available as a Docker image on `git.peterkeuter.nl:5033/peter/hostmonitorweb:latest`. To run use a command like this:
```
docker run -d --restart=always -p 8002:80 --name=hostmonitorweb -e DB_CONNECTION_STRING="server=db;uid=uid;pwd=pwd;database=db" -e HOSTMONITOR_HOSTNAME="hostmonitor.domain.local" -e HEARTBEAT_INTERVAL=120000 git.peterkeuter.nl:5033/peter/hostmonitorweb:latest
```

Additionally, there is a `docker-compose_example.yml`-file, which (if started) will automatically run a MySQL-server and this application side by side. 
The database will be exposed on the default port (_3306_). If this doesn't work, please check the firewall. Connection details are in the `docker-compose_example.yml`-file.
Default username/password/database are `hostmonitor`.

**NOTE** If you change the credentials, make sure you change them in both locations in this file (connection string and mysql-config)

## Configuration

 * `DB_CONNECTION_STRING`: the connection string to connect with the database. Example: `server=db;uid=uid;pwd=pwd;database=db`
 * `HOSTMONITOR_HOSTNAME`: Optional. This needs to be filled in when using the heartbeat-functionality. 
    The hostname of the HostMonitor instance to check if the heartbeat is valid, so this endpoint can only be used by HostMonitor.
    This hostname will also be used by the application to connect to HostMonitor on it's control port (_1054_), so make sure that port is opened in any firewall. 
    No data will be sent over this connection, it's just to check if HostMonitor is available.
 * `HEARTBEAT_INTERVAL`: Optional. This needs to be filled in when using the heartbeat-functionality. 
    The interval (in **milliseconds**) to check the heartbeat value. If no `POST` has been received on the API within this interval, the connection will be in an error-state.
    And the dashboard will show an error.
 * `DISABLE_HEARTBEAT_IP_CHECK`: Optional. Boolean field to decide if the IP-check in the heartbeat POST-endpoint should be disabled. If this is set to "true", every IP will be allowed to POST to this endpoint. 
 * `GROUP_BY_FULL_PATH`: Optional. If this boolean field is set to "true", it will group all items by the top level folder name, and not by the folder name the test is in.
 * `GREEN_ON_OK`: Optional. If this boolean field is set to "true", all "OK" items will be rendered with a green background.

## Database
The database will be need to setup with the following query:
```sql
CREATE TABLE IF NOT EXISTS `status` (
  `testid` int NOT NULL,
  `eventtime` datetime NOT NULL,
  `testname` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `full_path` text NOT NULL,
  `status` varchar(45) NOT NULL,
  `reply` varchar(255) DEFAULT NULL,
  `testmethod` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`testid`)
);
```

**NOTE** This table will be automatically created when the application starts and the table does not exist yet.

## HostMonitor
The query you can use to update the status table from HostMonitor is:
```sql
INSERT INTO status (folder, full_path, testname, status, eventtime, reply, testmethod, testid) VALUES ("%Folder%", "%FullPath%", "%TestName%", "%Status%", "%DateTime%", "%Reply%", "%TestMethod%", "%TestID%") ON DUPLICATE KEY UPDATE eventtime="%DateTime%", status="%Status%", reply="%Reply%", testmethod="%TestMethod%", testname="%TestName%"
```

### Notes
* Make sure that HostMonitor will output the dates as a ISO-conforming format like `yyyy-MM-dd HH:mm:ss`. You can set this in the settings of HostMonitor (Miscellaneous/Date&Time format).
* Make sure that in the hostmon.ini (%ProgramData%) in the [Misc] category,`ODBC_UseDoubleSlash=1` is set.
* If a test has been removed, or moved into another folder, it is necessary to delete the item manually to reflect the changes in the dashboard.