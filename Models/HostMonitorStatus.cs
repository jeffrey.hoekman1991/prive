﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HostMonitorWeb.Models
{
    public enum Statuses
    {
        Disabled,
        Ok,
        Warning,
        Errored
    }

    public class StatusItem
    {
        public string Folder { get; set; }
        public DateTime EventTime { get; set; }
        public string TestMethod { get; set; }
        public string Reply { get; set; }
        public string FullPath { get; set; }

        public string DisplayFolder
        {
            get
            {
                if (Environment.GetEnvironmentVariable("GROUP_BY_FULL_PATH") == "true")
                {
                    var splitted = FullPath?.Split("\\").Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    if (splitted.Count > 1 && splitted[0].Equals("Root", StringComparison.OrdinalIgnoreCase))
                    {
                        return splitted[1];
                    }
                    else if (splitted.Count == 0 || (splitted.Count == 1 && string.IsNullOrWhiteSpace(splitted[0])))
                    {
                        return null;
                    }
                    return splitted[0];
                }
                return Folder;
            }
        }

        public string Name { get; set; }
        public int Code { get; set; }
        public string Status { get; set; }

        public Statuses StatusVal
        {
            get
            {
                switch (Status)
                {
                    case "Bad":
                    case "Bad contents":
                    case "No answer":
                        return Statuses.Errored;
                    case "Warning":
                    case "Unknown":
                        return Statuses.Warning;
                    case "Ok":
                    case "Normal":
                    case "Host is alive":
                        return Statuses.Ok;
                    //case "Out of schedule":
                    //case "Disabled":
                    //case "Wait for master":
                    default:
                        return Statuses.Disabled;
                }
            }
        }
    }

    public class HostMonitorStatusReply
    {
        public HostMonitorStatusReply(List<HostMonitorStatus> status)
        {
            Status = status;
        }

        public bool GreenOnOk { get => Environment.GetEnvironmentVariable("GREEN_ON_OK") == "true"; }
        public List<HostMonitorStatus> Status { get; }
    }

    public class HostMonitorStatus
    {
        public string Name { get; set; }
        public string FullPath { get; set; }
        public Statuses Status { get => Items?.Max(x => x.StatusVal) ?? Statuses.Warning; }
        public List<StatusItem> Items { get; set; }
    }
}

