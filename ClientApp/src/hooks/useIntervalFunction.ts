﻿import { useEffect } from "react";

export const useIntervalFunction = (interval: number, func: Function) => {
    useEffect(() => {
        var id = setInterval(func, interval);
        return () => clearInterval(id);
    }, [interval, func]);
};