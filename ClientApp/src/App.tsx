import React from 'react';
import { hot } from 'react-hot-loader/root'
import { Home } from './components/Home';
import { Route } from 'react-router-dom';

function App() {
    return <Route exact path='/' component={Home} />;
};

export default process.env.NODE_ENV === "development" ? hot(App) : App
