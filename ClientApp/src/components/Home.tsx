import React, { useState, useEffect } from 'react';
import { chunk, differenceBy } from "lodash-es";
import axios from "axios";
import { Container, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';
import { useIntervalFunction } from '../hooks/useIntervalFunction';
import { HostMonitorStatus, Statuses, StatusItem, HostMonitorStatusReply } from '../models/StatusItem';


export function Home() {
    const [statuses, setStatuses] = useState<HostMonitorStatus[]>([]);
    const [available, setAvailable] = useState<boolean>(true);
    const [greenOnOk, setgreenOnOk] = useState<boolean>(false);
    const [modalData, setModalData] = useState<HostMonitorStatus | null>(null);

    const checkAlive = async () => {
        try {
            await axios.get("/api/hostmonitor/alive");
            if (!available) {
                const audio = new Audio('gnid.wav');
                audio.play();
            }
            setAvailable(true);
            return true;
        } catch (err) {
            if (available) {
                const audio = new Audio('ding.wav');
                audio.play();
            }
            setAvailable(false);
        }
        return false;
    }

    const updateData = async () => {
        if (!await checkAlive()) {
            return;
        }

        const { data } = await axios.get<HostMonitorStatusReply>("/api/hostmonitor");
        const currentErrored = statuses.flatMap(x => x.items.filter(x => x.statusVal === Statuses.Errored));
        const newErrored = data.status.flatMap(x => x.items.filter(x => x.statusVal === Statuses.Errored));
        if (newErrored > currentErrored) {
            const audio = new Audio('ding.wav');
            audio.play();
        } else if (newErrored < currentErrored) {
            const audio = new Audio('gnid.wav');
            audio.play();
        }
        const diff = differenceBy(data.status, statuses, x => x.status);
        for (const d of diff) {
            const currentStatus = statuses.find(x => x.name === d.name);
            const newStatus = data.status.find(x => x.name === d.name);
            if (currentStatus && newStatus?.status === Statuses.Ok && currentStatus?.status !== Statuses.Ok && currentStatus?.status !== Statuses.Fixed) {
                newStatus.status = Statuses.Fixed;
            }
        }
        setStatuses(data.status);
        setgreenOnOk(data.greenOnOk);
    }
    useIntervalFunction(10000, updateData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => { updateData(); }, []);

    const openMenu = (group: HostMonitorStatus) => {
        setModalData(group);
    }

    const closeModal = () => {
        setModalData(null);
    }

    const getClassName = (status: Statuses) => {
        switch (status) {
            case Statuses.Disabled:
                return "disabled";
            case Statuses.Warning:
                return "warning";
            case Statuses.Errored:
                return "error";
            case Statuses.Fixed:
                return "fixed";
            default:
                return "good";
        }
    }

    const buildClassName = (status: Statuses) => {
        const className = getClassName(status);
        if (!greenOnOk) {
            return className;
        }
        return `green-on-ok ${className}`;
    }

    const removeItem = async (item: StatusItem) => {
        await axios.delete(`/api/hostmonitor/${item.code}`);
        const { data } = await axios.get<HostMonitorStatus>(`/api/hostmonitor/${modalData?.fullPath}`);
        setModalData(data);
    }

    const renderModalItem = (item: StatusItem) => (
        <tr className={buildClassName(item.statusVal)} key={item.name}>
            <td>{item.name}</td>
            {item.folder !== item.displayFolder && <td>{item.fullPath}</td>}
            <td>{item.status}</td>
            <td className="btn btn-danger" onClick={() => removeItem(item)}>X</td>
        </tr>);

    const renderModal = () => (
        <Modal size="lg" centered isOpen={!!modalData} toggle={closeModal}>
            <ModalHeader>
                {modalData?.name}
            </ModalHeader>
            <ModalBody className="d-flex justify-content-center">
                <table className="table table-bordered">
                    <tbody>
                        {modalData?.items.map(renderModalItem)}
                    </tbody>
                </table>
            </ModalBody>
        </Modal>)

    const renderStatusBoard = () => (
        <table className="table table-bordered table-statusboard">
            <tbody>
                {chunk(statuses, 5).map((group, idx) => (<tr key={idx}>
                    {renderRow(group)}
                </tr>))}
            </tbody>
        </table>);

    const renderError = () => (
        <Row className="h-100 justify-content-center align-items-center error">
            <span>Connection to HostMonitor</span>
        </Row>);

    const renderRow = (group: HostMonitorStatus[]) => group.map(g => (
        <td className={`align-middle ${buildClassName(g.status)}`}
            onClick={() => openMenu(g)}
            key={g.name}>
            {g.name}
        </td>
    ));

    return (
        <Container fluid className="px-0">
            {available && renderModal()}
            {available && renderStatusBoard()}
            {!available && renderError()}
        </Container>
    );
};
