﻿export enum Statuses {
    Disabled,
    Ok,
    Warning,
    Errored,
    Fixed,
}

export interface StatusItem {
    folder: string;
    name: string;
    code: number;
    eventTime: Date;
    displayFolder: string;
    testMethod: string;
    fullPath: string;
    reply: string;
    status: string;
    statusVal: Statuses;
}

export interface HostMonitorStatus {
    name: string;
    fullPath: string;
    status: Statuses;
    items: StatusItem[];
}

export interface HostMonitorStatusReply {
    greenOnOk: boolean;
    status: HostMonitorStatus[];
}