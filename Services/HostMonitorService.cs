﻿using HostMonitorWeb.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace HostMonitorWeb.Services
{
    public class HostMonitorService
    {
        private readonly Timer t;
        private readonly int interval = -1;
        private readonly ILogger<HostMonitorService> _logger;
        private readonly IMemoryCache _cache;

        private bool hasValidHeartbeat = true;

        public HostMonitorService(ILogger<HostMonitorService> logger, IMemoryCache cache)
        {
            _logger = logger;
            _cache = cache;
            CheckOrCreateTable();
            if (int.TryParse(Environment.GetEnvironmentVariable("HEARTBEAT_INTERVAL"), out var interval))
            {
                this.interval = interval;
            }
            t = new Timer(_ => hasValidHeartbeat = false, null, this.interval, this.interval);
        }

        public void RemoveItems(string id)
        {
            using var connection = GetConnection();
            using var command = new MySqlCommand("DELETE FROM status WHERE testid=@id", connection);
            command.Parameters.AddWithValue("@id", id);
            command.ExecuteNonQuery();
        }

        private void CheckOrCreateTable()
        {
            using var connection = GetConnection();
            using var command = new MySqlCommand(@"
             CREATE TABLE IF NOT EXISTS `status` (
              `testid` int NOT NULL,
              `eventtime` datetime NOT NULL,
              `testname` varchar(255) NOT NULL,
              `folder` varchar(255) NOT NULL,
              `full_path` text NOT NULL,
              `status` varchar(45) NOT NULL,
              `reply` varchar(255) DEFAULT NULL,
              `testmethod` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`testid`)
            );", connection);
            command.ExecuteNonQuery();
        }

        public void SetHeartbeat()
        {
            hasValidHeartbeat = true;
            t.Change(interval, interval);
        }

        public bool IsHostMonitorAlive()
        {
            if (!hasValidHeartbeat)
            {
                return false;
            }
            var hostMonitorHostname = Environment.GetEnvironmentVariable("HOSTMONITOR_HOSTNAME");
            // testing the database connection, this will throw if the connection is unavailable
            try
            {
                GetItems();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to connect to MySQL database");
                return false;
            }
            if (string.IsNullOrWhiteSpace(hostMonitorHostname))
            {
                return true;
            }
            return _cache.GetOrCreate("tcpConnection", fac =>
            {
                fac.SetAbsoluteExpiration(TimeSpan.FromSeconds(20));
                using var tcpClient = new TcpClient();
                try
                {
                    tcpClient.Connect(hostMonitorHostname, 1054);
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Unable to connect to HostMonitor over TCP");
                }
                return false;
            });
        }

        private List<StatusItem> GetItemStatus(string fullPath = null)
        {
            using var connection = GetConnection();
            using var command = new MySqlCommand(null, connection)
            {
                CommandText = "SELECT * FROM (SELECT * FROM status ORDER BY eventtime DESC) AS a"
            };
            if (!string.IsNullOrWhiteSpace(fullPath))
            {
                command.CommandText += " WHERE full_path = @fullpath";
                command.Parameters.AddWithValue("@fullpath", fullPath);
            }
            command.CommandText += " GROUP BY testid;";
            using var da = new MySqlDataAdapter
            {
                SelectCommand = command
            };
            var dataset = new DataSet();
            da.Fill(dataset);
            return dataset.Tables[0].Rows.Cast<DataRow>()
                .Select(r => new StatusItem
                {
                    Code = (int)r["testid"],
                    EventTime = (DateTime)r["eventtime"],
                    Folder = (string)r["folder"],
                    FullPath = (string)r["full_path"],
                    TestMethod = (string)r["testmethod"],
                    Reply = (string)r["reply"],
                    Name = (string)r["testname"],
                    Status = (string)r["status"]
                })
                .Where(x => x.DisplayFolder != null)
                .ToList();
        }

        public HostMonitorStatus GetItem(string id)
        {
            var items = GetItemStatus(id);
            return new HostMonitorStatus
            {
                Name = items[0].DisplayFolder,
                FullPath = items[0].FullPath,
                Items = items.ToList(),
            };
        }

        public HostMonitorStatusReply GetItems()
        {
            var grouped = GetItemStatus().OrderBy(x => x.DisplayFolder).GroupBy(x => x.DisplayFolder);
            return new HostMonitorStatusReply(grouped.Select(group => new HostMonitorStatus
            {
                Name = group.Key,
                FullPath = group.FirstOrDefault()?.FullPath,
                Items = group.ToList(),
            }).ToList());
        }

        private MySqlConnection GetConnection()
        {
            var conn = new MySqlConnection(Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"));
            conn.Open();
            return conn;
        }

    }
}