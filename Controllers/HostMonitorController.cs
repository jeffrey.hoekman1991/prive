﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HostMonitorWeb.Models;
using HostMonitorWeb.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HostMonitorWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HostMonitorController : ControllerBase
    {
        private readonly HostMonitorService _service;
        private readonly ILogger<HostMonitorController> _logger;

        public HostMonitorController(ILogger<HostMonitorController> logger, HostMonitorService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpPost("heartbeat")]
        public IActionResult PostHeartbeat()
        {
            var disableHeartbeatIpCheck = Environment.GetEnvironmentVariable("DISABLE_HEARTBEAT_IP_CHECK")?.Equals("true") ?? false;
            var allowedIp = Dns.GetHostAddresses(Environment.GetEnvironmentVariable("HOSTMONITOR_HOSTNAME")).FirstOrDefault()?.MapToIPv4();
            var remoteIp = HttpContext.Connection.RemoteIpAddress.MapToIPv4();
            if (allowedIp == null || (!disableHeartbeatIpCheck && !remoteIp.Equals(allowedIp)))
            {
                _logger.LogWarning($"Disallowed heartbeat update from {remoteIp} when allowed IP is {allowedIp}");
                return BadRequest();
            }
            _service.SetHeartbeat();
            return Ok();
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            return Ok(_service.GetItems());
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(string id)
        {
            return Ok(_service.GetItem(id));
        }

        [HttpGet("alive")]
        public IActionResult GetAlive()
        {
            if (_service.IsHostMonitorAlive())
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteValue(string id)
        {
            _service.RemoveItems(id);
            return Ok();
        }
    }
}
